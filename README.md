# Sample test of automation for create a rocky lxc from official image and install and run podman service

## what do you need?

- proxmox VE installation
- internet connection
- a LXC/VM to use like infrastructure control center [ICC]
- ansible installed [icc]
- terraform installed [icc]

## what do I get at the end?

- 1 rocky LXC with podman environment ready

## what steps are necessary?

- upload on a pve node a official LXC rocky template
- create and modify a vars.tf files
- use terraform for create 1 LXC from the modified template
- execute ansible playbook on this LXC for create a podman environment

## how to get a alpine LXC template?

- `pveam update`
- `pveam available`
- `pveam download $storage_template $template_name`

## how to use terraform?

- `terraform -chdir=terraform init`
- `terraform -chdir=terraform plan`
- `terraform -chdir=terraform apply`

## how to use ansible?

- `ansible-playbook -i ansible/ansible_hosts.txt  ansible/ansible_lxc_rck_run_podman.yaml`
